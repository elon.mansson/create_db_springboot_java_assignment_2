# 👏 Small Rpg game

This is a small Java application that was a small assignment to learn a bit of SQL.

## ✌️ Description

### 🔥 About the application

This application add some functionallity to sort data from the diffrent tabels in an easy way.

The application contains the following functions:

* Read all the customers.

* Read a specific customer from id.

* Read a specific customer from name.

* Return a page of customers with params.

* Add a new customer to the database.

* Update a existing customer.

* Return the country with the most amount of customers.

* Get the customer with the highest combined invoice.

* Get a customer(with id) to get their most popular genre.

### 🤖 Technologies

* JAVA
* GIT
* SQL

## ⭐ Getting Started


### ⚠️ Dependencies

* We recommend to not change the versions in the dependencies. Otherwise, the application might not work. 

### ⚡ Installing

Follow the steps below to create and setup the translation application.

2. Clone the repo:
```
git clone https://gitlab.com/elon.mansson/create_db_springboot_java_assignment_2
``` 


### 💻 Executing program

* How to run the program:

```
Add the url, user, password for your datebase in the application.properties file.
```
```
Build the project with JDK 17.
```
```
Check in main how to run all the diffrent functions.
```

## 😎 Authors

Contributors and contact information

Elon Månsson 
[@Elon.Mansson](https://gitlab.com/elon.mansson/)

Robin Axelstrom
[@Robin.Axelstrom](https://gitlab.com/Robinax)

## 🦝 Version History

* 0.1
    * Initial Release

## 🌌 License

This project is fully free for use.
