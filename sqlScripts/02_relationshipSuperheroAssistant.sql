/*******************************************************************************
   Create Foreign Keys
********************************************************************************/

ALTER TABLE assistant
    ADD superhero_id int NOT NULL REFERENCES superhero;