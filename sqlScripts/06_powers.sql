/*******************************************************************************
   Populate Tables
********************************************************************************/

INSERT INTO power (power_name, power_desc) VALUES('STRONG', 'VERY STRONK');
INSERT INTO power (power_name, power_desc) VALUES('FAST', 'VERY FAST');

INSERT INTO superhero_power (superhero_id, power_id) VALUES(1, 1);
INSERT INTO superhero_power (superhero_id, power_id) VALUES(1, 2);
INSERT INTO superhero_power (superhero_id, power_id) VALUES(2, 1);
INSERT INTO superhero_power (superhero_id, power_id) VALUES(3, 2);