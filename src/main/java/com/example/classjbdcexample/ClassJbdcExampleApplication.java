package com.example.classjbdcexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassJbdcExampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(ClassJbdcExampleApplication.class, args);
    }

}
