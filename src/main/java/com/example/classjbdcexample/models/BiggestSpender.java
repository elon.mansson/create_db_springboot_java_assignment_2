package com.example.classjbdcexample.models;

//Model for biggest spender. This is not needed if we execute the sql code, but we tough it was fun to try
//With java. It was a challenge.
public record BiggestSpender(int id, String first_name, double total) {

}
