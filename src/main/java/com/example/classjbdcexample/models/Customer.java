package com.example.classjbdcexample.models;

//Model for the customer. All functions will use this model
public record Customer(int id, String first_name, String last_name, String company, String address, String country, String postal_code, String phone, String email) {

}
