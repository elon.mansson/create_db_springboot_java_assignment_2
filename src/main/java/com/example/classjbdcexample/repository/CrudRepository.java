package com.example.classjbdcexample.repository;

import java.util.List;

//Crud repo. all the functions exists in the customerrepositoryimpl
public interface CrudRepository<T, ID, NAME, LIMIT, OFFSET ,biggestSpender>{
    List<T> findAll();

    T findBiggestSpender();
    List<String> findMostCommonGenre(ID id);
    List<T> findSome(LIMIT limit, OFFSET offset);
    void createNewCustomer(T object);
    T findById(ID id);
    T findByName(NAME name);
    void updateById(T object);
}
