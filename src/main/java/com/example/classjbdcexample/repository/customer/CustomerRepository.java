package com.example.classjbdcexample.repository.customer;

import com.example.classjbdcexample.models.BiggestSpender;
import com.example.classjbdcexample.models.Customer;
import com.example.classjbdcexample.repository.CrudRepository;

//Customers interface repo
public interface CustomerRepository extends CrudRepository<Customer, Integer, String, Integer, Integer, BiggestSpender> {

}
