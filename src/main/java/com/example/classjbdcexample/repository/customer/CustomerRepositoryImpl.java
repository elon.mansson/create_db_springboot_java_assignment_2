package com.example.classjbdcexample.repository.customer;

import com.example.classjbdcexample.models.BiggestSpender;
import com.example.classjbdcexample.models.Customer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository{

    private String url;
    private String username;
    private String password;
    //Taking the url/username/password from the application.properties.
    public CustomerRepositoryImpl(@Value("${spring.datasource.url}")String url,
            @Value("${spring.datasource.username}")String username,
            @Value("${spring.datasource.password}")String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    //Function to find all customers
    @Override
    public List<Customer> findAll() {
        List customers = new ArrayList<>();
        Customer customer = null;
        String sql = "SELECT customer_id ,first_name, last_name, company, address, country, postal_code,phone,email FROM customer";
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("company"),
                        resultSet.getString("address"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customers.add(customer);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return customers;
    }

    //Function to find the biggest spender. We did this with a hashmap and later found a sql solution
    //that would have made our life a lot easier, so we taught it was a fun challenge, so we kept it in.
    @Override
    public Customer findBiggestSpender() {
        double higestInvoice = 0d;
        int costumerKey = 0;
        /*We also found a solution with sql instead of the plain java
        SELECT first_name, SUM (invoice.total) AS total FROM customer
        INNER JOIN invoice on invoice.customer_id = customer.customer_id
        GROUP BY customer.customer_id
        ORDER BY total desc
        FETCH FIRST 1 ROW WITH TIES
         */
        BiggestSpender biggestSpenders = null;
        HashMap<Integer,Double> findbiggestspender = new HashMap<>();
        String sql = "SELECT * FROM customer A INNER JOIN invoice B ON A. customer_id =B. customer_id";
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                biggestSpenders = new BiggestSpender(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getDouble("total")
                );

                if (findbiggestspender.containsKey(biggestSpenders.id())) {
                    findbiggestspender.put(biggestSpenders.id(), findbiggestspender.get(biggestSpenders.id()) + biggestSpenders.total());
                } else {
                    findbiggestspender.put(biggestSpenders.id(), biggestSpenders.total());
                }
            }

            for (int i = 1; i < findbiggestspender.size(); i++) {
                if(findbiggestspender.get(i) > higestInvoice && findbiggestspender.get(i) != null){
                    higestInvoice = findbiggestspender.get(i);
                    costumerKey = i;
                }
            }

        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return findById(costumerKey);
    }

    //Function to find the most common genre
    @Override
    public List<String> findMostCommonGenre(Integer id) {
        List result = new ArrayList<>();
        String sql = "SELECT COUNT(genre.name), genre.name FROM (((invoice INNER JOIN invoice_line ON invoice.invoice_id = invoice_line.invoice_id) INNER JOIN track ON invoice_line.track_id = track.track_id) INNER JOIN genre ON track.genre_id = genre.genre_id) WHERE customer_id = ? GROUP by genre.name ORDER BY count(*) desc FETCH FIRST 1 ROWS WITH TIES";
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                result.add(resultSet.getString(1));
                result.add(resultSet.getString(2));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return result;
    }

    //Function to find customer by limit and offset.
    @Override
    public List<Customer> findSome(Integer limit, Integer offset) {
        List customers = new ArrayList<>();
        Customer customer = null;
        String sql = "SELECT customer_id ,first_name, last_name, company, address, country, postal_code,phone,email FROM customer LIMIT ? OFFSET ?";
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("company"),
                        resultSet.getString("address"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customers.add(customer);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return customers;
    }


    //Function to create new customers
    @Override
    public void createNewCustomer(Customer customer) {
        String sql = "INSERT INTO customer (first_name, last_name, company, address, country, postal_code, phone, email) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.first_name());
            preparedStatement.setString(2, customer.last_name());
            preparedStatement.setString(3, customer.company());
            preparedStatement.setString(4, customer.address());
            preparedStatement.setString(5, customer.country());
            preparedStatement.setString(6, customer.postal_code());
            preparedStatement.setString(7, customer.phone());
            preparedStatement.setString(8, customer.email());
            preparedStatement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    //Function to find customer by id
    @Override
    public Customer findById(Integer id) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("company"),
                        resultSet.getString("address"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return customer;
    }

    //Function to find customer by id
    @Override
    public Customer findByName(String name) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE first_name = ?";
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("company"),
                        resultSet.getString("address"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return customer;
    }


    //Function to update customer by id
    @Override
    public void updateById(Customer customer) {
        String sql = "UPDATE customer SET first_name = ? , last_name = ? , company = ? , address = ? , country = ? , postal_code = ? , phone = ? , email = ?  WHERE customer_id = ?";
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.first_name());
            preparedStatement.setString(2, customer.last_name());
            preparedStatement.setString(3, customer.company());
            preparedStatement.setString(4, customer.address());
            preparedStatement.setString(5, customer.country());
            preparedStatement.setString(6, customer.postal_code());
            preparedStatement.setString(7, customer.phone());
            preparedStatement.setString(8, customer.email());
            preparedStatement.setInt(9, customer.id());
            preparedStatement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
