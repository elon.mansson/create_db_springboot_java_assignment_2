package com.example.classjbdcexample.runners;

import com.example.classjbdcexample.models.Customer;
import com.example.classjbdcexample.repository.customer.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class CustomerRunner implements ApplicationRunner {

    private final CustomerRepository customerRepository;

    public CustomerRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

       //       Finds customer by id
       // System.out.println(customerRepository.findById(1));
       //       Finds customer by name
        // System.out.println(customerRepository.findByName("Roberto"));
                // Write out all customers
       //  System.out.println(customerRepository.findAll());
                //Finds customer with limit and offset
      //  System.out.println(customerRepository.findSome(3, 5));
       //               Creates a new customer
     //   customerRepository.createNewCustomer(new Customer(1, "SEAN", "månsson", "asd", "asd", "asd", "asd", "asd", "asd"));
                    // Just checking if it worked.
     //   System.out.println(customerRepository.findByName("SEAN"));
    //                  Updates a customer by id
      //  customerRepository.updateById(new Customer(61, "SEAN", "månsson", "asd", "asd", "asd", "asd", "asd", "asd"));
                //Finds the biggest spender in the invoice total.
     //   System.out.println(customerRepository.findBiggestSpender());
                    // Finds the most common genre
     //   System.out.println(customerRepository.findMostCommonGenre(12));

    }
}
